# FLAC to MP3 Converter

This script extracts ZIP Files and converts FLAC audio files to MP3 files using `ffmpeg`.

## Features

- Extracts FLAC files from zip files in the input directory
- Converts FLAC files to MP3 using the `libmp3lame` codec
- Saves the converted files to the output directory
- Uses multiple processes to convert files in parallel

## Installation

1. Install `ffmpeg` if it is not already installed on your system.
2. Clone or download the repository.

## Usage

1. Set the input and output directories in the script.
2. Run the script: `python main.py`

## Known Issues

- If the output files already exist, the script will overwrite them by default.

## Credits

- This script uses `ffmpeg` to convert the audio files.
- This script and README was mostly written by ChatGPT!
