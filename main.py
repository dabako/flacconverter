import os
import subprocess
import zipfile
import tempfile
import multiprocessing

# Set the input and output directories
input_dir = '/home/dabako/Downloads/test'
output_dir = '/home/dabako/Downloads/test_o'

# Create a temporary directory
temp_dir = tempfile.TemporaryDirectory()

def convert_file(input_path):
    """Convert a FLAC file to an MP3 file"""
    # Extract the filename from the input path
    input_file = os.path.basename(input_path)
    # Print the message
    print(f'Converting {input_file} to mp3')
    # Construct the full path to the output file
    output_path = os.path.join(zip_folder, input_file[:-5] + '.mp3')
    # Call ffmpeg to convert the file
    result = subprocess.run(['ffmpeg', '-y', '-i', input_path, '-c:a', 'libmp3lame', '-q:a', '0', output_path], stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)
    if result.returncode != 0:
        return f'Error converting {input_file} to mp3'

# Iterate over all the zip files in the input directory
for zip_file in os.listdir(input_dir):
    # Check if the file is a zip file
    if zip_file.endswith('.zip'):
        # Extract the zip file to the temporary directory
        try:
            with zipfile.ZipFile(os.path.join(input_dir, zip_file)) as zip_obj:
                zip_obj.extractall(temp_dir.name)
        except (FileNotFoundError, zipfile.BadZipFile) as e:
            print(f'Error opening zip file {zip_file}: {e}')
            continue
        # Create a folder for the zip file
        zip_folder = os.path.join(output_dir, zip_file[:-4])
        try:
            os.makedirs(zip_folder, exist_ok=True)
        except (FileNotFoundError, PermissionError) as e:
            print(f'Error creating output directory {zip_folder}: {e}')
            continue
        # Iterate over all the files in the temporary directory
        flac_files = [os.path.join(temp_dir.name, file) for file in os.listdir(temp_dir.name) if file.endswith('.flac')]
        # Use a process pool to convert the FLAC files in parallel
        with multiprocessing.Pool() as pool:
            for result in pool.imap_unordered(convert_file, flac_files):
                if result is not None:
                    print(result)
        # Remove the files from the temporary directory
        for file in os.listdir(temp_dir.name):
            os.remove(os.path.join(temp_dir.name, file))

# Delete the temporary directory
temp_dir.cleanup()
